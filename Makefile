# Detecting platform (win or unix)
VENV = if [ -d ".venv/Scripts" ]; then \
			. .venv/Scripts/activate; \
		else \
			. .venv/bin/activate; \
		fi
PIP := ${VENV} && pip
POETRY := ${VENV} && poetry
PYTEST := ${VENV} && pytest

venv:
	if [ ! -d ".venv" ]; then \
		python -m venv .venv \
		&& ${PIP} install poetry; \
	fi;

install: venv
	${POETRY} install

build: install
	${POETRY} build

test:
	${PYTEST} .

clean_pyc:
	-find -iname "*.pyc" -delete

clean_venv:
	-rm -rf .venv

clean_package:
	-rm -rf hello.egg-info

clean: clean_venv clean_package clean_pyc