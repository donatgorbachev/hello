from hello import __version__
from hello import hello_world
from hello.constants import HELLO_WORLD_STING


def test_version():
    assert __version__ == '0.1.0'


def test_hello_world():
    result = hello_world()
    assert result == HELLO_WORLD_STING
