# Hello World project

## Theory

This is example project. You should use `unix` or `git-bash` (`sh`) to run commands.

## Getting started

The project uses `poetry` and `make`.

### Creating virtual environment

```shell_script
make venv
```

### Installing dependencies

```shell_script
make install
```

### Testing

```shell_script
make test
```

### Building the package

```shell_script
make build
```

### CI Pipeline

* testing (`make test`)
* building (`make build`, returns artifacts)
